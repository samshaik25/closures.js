function counterFactory()
{
    var value=1;
    return {
        increment :function()
        {
            return ++value;
        },
	    decrement :function()
	    {
		    return --value;
	    }
    };
}

module.exports.counterFactory=counterFactory;
