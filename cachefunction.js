function  cache(cb)
   {  
     const cache={};
     return function(n)
     {
       if(cache[n]!=undefined)
       {
       return cache[n];
       }
       else 
       {
         result=cb(n);
         cache[n]=result;
         return result;
       }
     }

   }
