var { counterFactory }=require('../counterfactory.js')

var testCF=counterFactory();
testCF.increment();                   // increment value
let i=testCF.increment();             // again increments value
console.log(i);                       // prints the value

let d=testCF.decrement();                    // decrements the value 
console.log(d);
